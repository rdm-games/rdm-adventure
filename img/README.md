# Image credits and licensing

The images in this directory are the work of various artists, and those that
are not in the public domain are used under licence.

## Images with a CC0 1.0 Universal (CC0 1.0) Public Domain Dedication

  - [bag-of-money.svg](https://thenounproject.com/term/bag-of-bills/192242/)
    is by Noun Project user MC.

## Images under the Creative Commons Attribution 3.0 United States (CC BY 3.0 US) Licence

  - [approved.svg](https://thenounproject.com/term/approved/2312895/)
    is by Noun Project user DailyPM.

  - [burning-building.svg](https://thenounproject.com/term/burning-building/1696082/)
    is by Noun Project user Peter van Driel.

  - [data-collection.svg](https://thenounproject.com/term/data-collection/2475086/)
    is by Noun Project user Becris.

  - [facepalm.svg](https://thenounproject.com/term/facepalm/18486/)
    is by Noun Project user Edward Boatman.

  - [give-up.svg](https://thenounproject.com/term/give-up/1715962/)
    is by Noun Project user Adrien Coquet.

  - [microscope.svg](https://thenounproject.com/term/electron/2642772/)
    is by Noun Project user Eucalyp.

  - [telephone.svg](https://thenounproject.com/term/telephone/23451/)
    is by Noun Project user Hayley Warren.

  - **victory.svg** is a composite of three images:

      - ‘[Goal](https://thenounproject.com/term/goal/1715960/)’
        by Noun Project user Adrien Coquet.

      - ‘[Fireworks (642291)](https://thenounproject.com/term/fireworks/642291/)’
        by Noun Project user Gan Khoon Lay.

      - ‘[Fireworks (642292)](https://thenounproject.com/term/fireworks/642292/)’
        by Noun Project user Gan Khoon Lay.

## Images under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International Licence

  - **cover.png** is by Adrian Nardone and David Stacey, and is included in the
    wider licensing regime of the game.
