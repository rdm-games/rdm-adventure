#! /bin/sh

# This patch adds accessibility features to the Harlowe story format.

# Create a new empty directory, and copy this script and the upstream
# Harlowe "format.js" file into it. (Do not operate directly on your
# working copy, in case of errors.)

# Run the script to rename your input "format.js" to "format-orig.js"
# and generate a new "format.js" with the patched code in place.

if [ ! -f "format.js" ]; then
  echo "Input format.js file not found."
  exit 1
fi

# A. Backup the upstream file:

cp format.js format-orig.js

# B. Patch the file in place with sed. Expressions:
#
#   1.  Add `role="link"` attribute to <tw-link> elements that already have
#       `tabindex`. The quotes have to be escaped quite a lot:
#       - because they occur in a JavaScript string: " -> \"
#       - because the JavaScript string occurs in the string value of the
#         "source" key of a JSON object: \ " -> \\ \"
#       - because backslashes have to escaped in regex: \ \ \ " -> \\ \\ \\ "
#       - because backslashes and quotes need to be escaped in the shell:
#         \ \ \ \ \ \ " -> \\ \\ \\ \\ \\ \\ \"
#
#   2.  Add `role="link" tabindex="0"` attributes to <tw-link> elements that
#       do not already have `tabindex`.
#
#   3.  Add `role="dialog" aria-modal="true"` to <tw-dialog>.
#
#   4.  Add `role="link"` to clickable <tw-enchantment> elements.
#       The quotes mark JavaScript string boundaries, so only have to be escaped
#       - because the JavaScript string occurs in the string value of the
#         "source" key of a JSON object: " -> \"
#       - because backslashes have to escaped in regex: \ " -> \\ "
#       - because backslashes and quotes need to be escaped in the shell:
#         \ \ " -> \\ \\ \"
#       The braces only have to be escaped once (for the regex).

sed -i -E \
  -e "s/<tw-link (style='[^']+' )?tabindex/<tw-link \1role=\\\\\\\\\\\\\"link\\\\\\\\\\\\\" tabindex/g" \
  -e "s/<tw-link>/<tw-link role=\\\\\\\\\\\\\"link\\\\\\\\\\\\\" tabindex=0>/g" \
  -e "s/<tw-dialog>/<tw-dialog role=\\\\\\\\\\\\\"dialog\\\\\\\\\\\\\" aria-modal=\\\\\\\\\\\\\"true\\\\\\\\\\\\\">/g" \
  -e "s/\{tabIndex:\\\\\"0\\\\\"\}/{role:\\\\\"link\\\\\",tabIndex:\\\\\"0\\\\\"}/" \
  format.js
