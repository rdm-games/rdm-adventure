SHELL = bash
NAME = rdm-game
LVLS = 1 2 3 4 5 6

.PHONY: clean

all: public/index.html

# This will list all the official variant versions.
variants: public/fr/index.html

#
# Main game
#
public:
	mkdir -p "$@"

public/img: public
	[ -d "$@" ] || ( cd "$<" && ln -s ../img )

public/index.html: public/img en/$(NAME).tw $(LVLS:%=en/level-%.tw) storyformats/harlowe-3.3.3/format.js
	tweego -o "$@" en/

#
# Translations
#
# - French (currently just an example as the files are missing)
public/fr: public
	mkdir -p "$@"

public/fr/img: public/fr
	[ -d "$@" ] || ( cd "$<" && ln -s ../img )

public/fr/index.html: public/fr/img fr/$(NAME).tw $(LVLS:%=fr/level-%.tw) storyformats/harlowe-3.3.3/format.js
	tweego -o "$@" fr/

#
# Institutional variants
#
# - Monolingual example
public/bath: public
	mkdir -p "$@"

public/bath/img: public/bath
	[ -d "$@" ] || ( cd "$<" && ln -s ../img )

public/bath/index.html: public/bath/img en/$(NAME).tw $(LVLS:%=en/level-%.tw) custom/bath.tw storyformats/harlowe-3.3.3/format.js
	sed -i 's/(display: "Guidance")/(display: "Guidance Bath")/' en/$(NAME).tw
	tweego -o "$@" en/ custom/bath.tw
	sed -i 's/(display: "Guidance Bath")/(display: "Guidance")/' en/$(NAME).tw

# - Multilingual example
#   - English
public/stellenbosch/en: public
	mkdir -p "$@"

public/stellenbosch/en/img: public/stellenbosch/en
	[ -d "$@" ] || ( cd "$<" && ln -s ../../img )

public/stellenbosch/en/index.html: public/stellenbosch/en/img en/$(NAME).tw $(LVLS:%=en/level-%.tw) custom/stellenbosch.tw storyformats/harlowe-3.3.3/format.js
	sed -i 's/(display: "Guidance")/(display: "Guidance Stellenbosch EN")/' en/$(NAME).tw
	tweego -o "$@" en/ custom/stellenbosch.tw
	sed -i 's/(display: "Guidance Stellenbosch EN")/(display: "Guidance")/' en/$(NAME).tw

#   - Afrikaans (currently just an example as the files are missing)
public/stellenbosch/af: public
	mkdir -p "$@"

public/stellenbosch/af/img: public/stellenbosch/af
	[ -d "$@" ] || ( cd "$<" && ln -s ../../img )

public/stellenbosch/af/index.html: public/stellenbosch/af/img af/$(NAME).tw $(LVLS:%=af/level-%.tw) custom/stellenbosch.tw storyformats/harlowe-3.3.3/format.js
	sed -i 's/(display: "Guidance")/(display: "Guidance Stellenbosch AF")/' af/$(NAME).tw
	tweego -o "$@" af/ custom/stellenbosch.tw
	sed -i 's/(display: "Guidance Stellenbosch AF")/(display: "Guidance")/' af/$(NAME).tw

clean:
	rm -f public/index.html
