# RDM Adventure

## Compiling

To compile the game you need to have a Twee 3 compiler installed. We have
tested it with Tweego but Extwee should also work.

[Tweego]: http://www.motoslave.net/tweego/
[Extwee]: https://github.com/videlais/extwee

It uses a patched version of the Harlowe v3.3.3 story format. Harlowe is
copyright (c) 2022 Leon Arnott, and our patched version is distributed under
licence. See the `harlowe-3.3.3` folder for more details about the licence and
the patch applied.

To generate the original (English-language) game in the current directory:

```bash
tweego -o index.html en/
```

The `make` utility installed, you can do some fancier things:

```bash
# Generate all versions of the game in a `public` folder:
make

# Remove generated files:
make clean
```

## Licence

This work (except where otherwise stated) is copyright (c) 2020 University of
Bath and Stellenbosch University, and is licensed under a [Creative Commons
Attribution-NonCommercial-ShareAlike 4.0 International
Licence](https://creativecommons.org/licenses/by-nc-sa/4.0/).
