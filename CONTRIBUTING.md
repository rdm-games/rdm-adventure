# Contributing to the RDM Adventure

## Translations

We welcome contributions to translate the RDM Adventure into other languages.

If you would like to contribute a translation, please create a fork of this
repository and make your changes as follows:

 1. Create a folder named after the ISO 639-1 code for the language,
    e.g. `cy` for Welsh.

 2. Copy the contents of the `en` folder into your new folder.

 3. Work through the files, translating the English strings into the new
    language. Please do not translate the names of variables; and obviously the
    Harlowe markup language cannot be translated either. You can introduce new
    variables as needed in order to handle grammatical issues that do not
    occur in English. It will help with future maintenance if you can keep
    to the same line breaks, so the line numbers match across translations.

 4. Compile and play the game to ensure it still works as in the original.

 5. Issue a merge (pull) request so we can take a look at it.

We will then update our build pipeline to make the translation available at
a URL like this:

  - `https://rdm-games.gitlab.io/rdm-adventure/[language-code]`

## Custom versions with guidance links

The credits screen of the game contains links to guidance on Research Data
Management:

> If you would like to know more about Research Data Management, please visit
> the guidance pages of your own institution. Here are the ones that we provide:
>
> - University of Bath Library guidance on research data
> - Stellenbosch University Library guidance on SUNScholarData and supporting services

If you would like to point your researchers to a version of the game where
that page provides a direct link to your own institutional guidance, please
create a fork of this repository and make your changes as follows.

 1. In the `custom` folder, make a copy of one of the files there and name it
    after your institution, e.g. `foobar.tw` for Foobar University. Copy
    `bath.tw` if you only want to customise the English language version. If you
    think you might want to customise multiple languages as they become
    available, copy `stellenbosch.tw` instead.

 2. In your `.tw` file, rename the passage(s) to include an institutional
    identifier, for example:

    ```
    :: Guidance Foobar
    ```

    If targeting multiple languages, include the language code:

    ```
    :: Guidance Foobar EN
    ```

    These passage names must not conflict with any of the passage names used
    in the game.

 3. Change the text of the passage as required.

 4. Issue a merge (pull) request so we can take a look at it.

We will then update our build pipeline to make the customised version(s)
available at URLs that look like one of the following:

  - `https://rdm-games.gitlab.io/rdm-adventure/[institution]`
  - `https://rdm-games.gitlab.io/rdm-adventure/[institution]/[language-code]`

It will be up to you to notify us if the link you provide needs to be updated.
We will not monitor it ourselves.
